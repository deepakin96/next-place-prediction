package com.prop.nextplaceprediction;




import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {
	Button lbs,climate,video,alarm,feedback,video1,dailyadd,dailyview,mapview;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		lbs=(Button)findViewById(R.id.button);
		climate=(Button)findViewById(R.id.button2);
		
		dailyadd=(Button)findViewById(R.id.Button01);
		dailyview=(Button)findViewById(R.id.Button02);
		mapview=(Button)findViewById(R.id.Button03);
		
		dailyadd.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				startActivity(new Intent(MainActivity.this,AddDailyActivity.class));		
				
			}
			
		});
		
		dailyview.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				startActivity(new Intent(MainActivity.this,ViewDailyActivity.class));		
				
			}
			
		});
		
		lbs.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				startActivity(new Intent(MainActivity.this,ServiceEntry.class));		
				
			}
			
		});
		
		climate.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				startActivity(new Intent(MainActivity.this,MainActivity2.class));		
				
			}
			
		});
		
		mapview.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				startActivity(new Intent(MainActivity.this,MapPage1.class));		
				
			}
			
		});
		
		
		
	/*	feedback.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent homeActivity=new Intent(MainActivity.this,Login.class);
				startActivity(homeActivity);		
				
			}
			
		}); */
	}

	

}
