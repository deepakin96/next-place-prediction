package com.prop.nextplaceprediction;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;

import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class SignUpActivity extends Activity {

	EditText edtName, edtMobileNo, edtEmail, edtCity, edtCountry, edtUserName,
	edtPassword, edtConfrimPassword;
Button btnSubmit;
Connection conn;
Double lat,lon;
static Cipher cipher;
protected LocationManager mlocManager;

private String name, mobilenumber, email, city, country, username,
	password, confrimpassword,lati,longi;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_up);
		edtCity = (EditText) findViewById(R.id.signup_editText_city);
		edtConfrimPassword = (EditText) findViewById(R.id.signup_editText_confirm_password);
		edtCountry = (EditText) findViewById(R.id.signup_editText_country);
		edtEmail = (EditText) findViewById(R.id.signup_editText_email);
		edtMobileNo = (EditText) findViewById(R.id.signup_editText_mobileNumber);
		edtName = (EditText) findViewById(R.id.signup_editText_name);
		edtPassword = (EditText) findViewById(R.id.signup_editText_password);
		edtUserName = (EditText) findViewById(R.id.signup_editText_username);
		
		mlocManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

		LocationListener mlocListener = new MyLocationListener();


		mlocManager.requestLocationUpdates( LocationManager.GPS_PROVIDER, 1, 1000, mlocListener);

//		lati=String.valueOf(lat);
//		longi=String.valueOf(lon);
//		if(lati.equals(null)||longi.equals(null) || lati.equals("") || longi.equals("")){
//			lati="71.000112";
//			longi="92.123343";
//		}
		btnSubmit = (Button) findViewById(R.id.signup_button_signup);
		btnSubmit.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				name = edtName.getText().toString();
				mobilenumber = edtMobileNo.getText().toString();
				email = edtEmail.getText().toString();
				city = edtCity.getText().toString();
				country = edtCountry.getText().toString();
				username = edtUserName.getText().toString();
				password = edtPassword.getText().toString();
				confrimpassword = edtConfrimPassword.getText().toString();
				
				Location location = mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
				
				if(location != null){
					lati = String.valueOf(location.getLatitude());
					longi = String.valueOf(location.getLongitude());
				}
				if(verify())
				{
						try {
							new QuerySQL().execute();
							
//							Statement statement = conn.createStatement();
//							int success=statement.executeUpdate("insert into register values('"+name+"','"+mobilenumber+"','"+email+"','"+city+"','"+country+"','"+username+"','"+password+"','"+lati+"','"+longi+"')");
//						
//							if (success >= 1) {
//								// successfully created product
//								Intent i = new Intent(getApplicationContext(),
//										LoginFormActivity.class);
//								startActivity(i);
	//
//								// closing this screen
//								finish();
//							} else {
//								// failed to create product
//							}
							} catch (Exception e) {
				        Log.e("ERRO",e.getMessage());
						}

			}
			}
		});

	}
	
//	private Connection CONN()
//	{
//
//		Connection conn = null;
//		String ConnURL = null;
//		try {
//		
//			
//			Class.forName("com.mysql.jdbc.Driver");
//			conn = DriverManager.getConnection("jdbc:mysql://ec2-23-21-211-172.compute-1.amazonaws.com:3306/eshadow","eshadowroot","password");				
//		} catch (SQLException se) {
//			Log.e("ERRO1",se.getMessage());
//		} catch (ClassNotFoundException e) {
//			Log.e("ERRO2",e.getMessage());
//		} catch (Exception e) {
//		    Log.e("ERRO3",e.getMessage());
//		}
//		return conn;
//	}

	public boolean verify()
	{
//		EditText name, userName, password, cpassword, email, phoneNumber;
		Boolean ret=true;
		if(edtName.getText().toString().length()<1){edtName.setError("Field Required");ret=false;}
		if(edtUserName.getText().toString().length()<1){edtUserName.setError("Field Required");ret=false;}
		if(edtPassword.getText().toString().length()<1){edtPassword.setError("Field Required");ret=false;}
		if(edtConfrimPassword.getText().toString().length()<1){edtConfrimPassword.setError("Field Required");ret=false;}
		if(!edtPassword.getText().toString().equals(edtConfrimPassword.getText().toString())){edtPassword.setError("Password not same");ret=false;}
		if(edtCity.getText().toString().length()<1){edtCity.setError("Field Required");ret=false;}
		if(edtCountry.getText().toString().length()<1){edtCountry.setError("Field Required");ret=false;}
		
		if(!edtEmail.getText().toString().contains("@")){edtEmail.setError("E-Mail ID Invalid");ret=false;}
		if(edtEmail.getText().toString().length()<1){edtEmail.setError("Field Required");ret=false;}
		
		char result = mobilenumber.charAt(0);
		int count=0;
		
		
		if(result!='9'){ count++;}//It will Set but ok it wont be visible
		if(result!='8'){ count++;}//It will Set but ok it wont be visible
		if(result!='7'){ count++;}//It will Set but ok it wont be visible
		
		if(count!=2)
		{
			edtMobileNo.setError("Invalid Phone Number "+count );
			ret=false;
		}
		
		if(edtMobileNo.getText().toString().length()<10){edtMobileNo.setError("Invalid Phone Number");ret=false;}//It will Set but ok it wont be visible
		if(edtMobileNo.getText().toString().length()<1){edtMobileNo.setError("Field Required");ret=false;}
		
		String expression = "^([0-9\\+]|\\(\\d{0,1}\\))[0-9\\-\\. ]{0,15}$";
        CharSequence inputString = edtMobileNo.getText().toString();
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(inputString);
        if (matcher.matches())
        {
		
        }
        else
        {
        	edtMobileNo.setError("Invalid Number");ret=false;
        }
		
		
		return ret;
	}
	

	
	
	public class QuerySQL extends AsyncTask<String, Void, Boolean> {

		ProgressDialog pDialog ;
		Exception error;
		ResultSet rs;
	    @Override
	    protected void onPreExecute() {
	        super.onPreExecute();
	        
	        pDialog = new ProgressDialog(SignUpActivity.this);
	        pDialog.setTitle("Registration");
	        pDialog.setMessage("Registering your credentials...");
	        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	        pDialog.setIndeterminate(false);
	        pDialog.setCancelable(false);
	        pDialog.show();
	    }

	    @Override
	    protected Boolean doInBackground(String... args) {
	    	
	    	
	    	
	    	Location location = mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			
			if(location != null){
				lati = String.valueOf(location.getLatitude());
				longi = String.valueOf(location.getLongitude());
			}
			
			try {
				
				
				Class.forName("com.mysql.jdbc.Driver");
				conn = DriverManager.getConnection("jdbc:mysql://103.10.235.220:3306/nextplace","root","password");		
			} catch (SQLException se) {
				Log.e("ERRO1",se.getMessage());
			} catch (ClassNotFoundException e) {
				Log.e("ERRO2",e.getMessage());
			} catch (Exception e) {
			    Log.e("ERRO3",e.getMessage());
			}
			

			try {
				Statement statement = conn.createStatement();
				int success=statement.executeUpdate("insert into register values('"+name+"','"+mobilenumber+"','"+email+"','"+city+"','"+country+"','"+username+"','"+password+"','"+lati+"','"+longi+"')");
				
				String work = "work";
				
				Statement statement2 = conn.createStatement();
				int success2=statement2.executeUpdate("insert into activitytable values('"+username+"','"+work+"','"+work+"','"+work+"','"+work+"','"+work+"','"+work+"')");
				
			
				if (success >= 1) {
					// successfully created product
					
					return true;
					// closing this screen
//					finish();
				} else {
					// failed to create product
					return false;
				}


				
				// Toast.makeText(getBaseContext(),
				// "Successfully Inserted.", Toast.LENGTH_LONG).show();
			} catch (Exception e) {
				error = e;
				return false;
//				Toast.makeText(getBaseContext(),"Successfully Registered...", Toast.LENGTH_LONG).show();
			}


	    }

	    @SuppressLint("NewApi")
		@Override
	    protected void onPostExecute(Boolean result1) {
	    	pDialog.dismiss ( ) ;
	    	if(result1)
	    	{
                
	    		Toast.makeText(getBaseContext(),"Successfully created your credentials." ,Toast.LENGTH_LONG).show();
					
//					System.out.println("ELSE(JSON) LOOP EXE");
					try {// try3 open
						
						Intent i = new Intent(getApplicationContext(),
								LoginFormActivity.class);
						startActivity(i);		
						
					} catch (Exception e1) {
						Toast.makeText(getBaseContext(), e1.toString(),
								Toast.LENGTH_LONG).show();

					}					
				
            
	    	}else
	    	{
	    		if(error!=null)
	    		{
	    			Toast.makeText(getBaseContext(),error.getMessage().toString() ,Toast.LENGTH_LONG).show();
	    			Log.d("Error not null...", error.getMessage().toString());
	    		}
	    		else
	    		{
	    			Toast.makeText(getBaseContext(),"Not crreated your credentials!!!" ,Toast.LENGTH_LONG).show();
	    		}
	    	}
	    	super.onPostExecute(result1);
	    }
	}

/* Class My Location Listener */

public class MyLocationListener implements LocationListener

{

public void onLocationChanged(Location loc)

{

loc.getLatitude();

loc.getLongitude();

String Text = "My current location is: " +"latitude = "+ loc.getLatitude() + "Longitude = " + loc.getLongitude();

lat=loc.getLatitude();
 lon=loc.getLongitude();

Toast.makeText( getApplicationContext(),Text,Toast.LENGTH_SHORT).show();

}




public void onProviderDisabled(String provider)

{

Toast.makeText( getApplicationContext(),"Gps Disabled",Toast.LENGTH_SHORT ).show();

}




public void onProviderEnabled(String provider)

{

Toast.makeText( getApplicationContext(), "Gps Enabled", Toast.LENGTH_SHORT).show();

}


public void onStatusChanged(String provider, int status, Bundle extras)

{


}



}/* End of Class MyLocationListener */
public static byte[] encryptText(String plainText,SecretKey secKey) throws Exception{

        // AES defaults to AES/ECB/PKCS5Padding in Java 7

        Cipher aesCipher = Cipher.getInstance("AES");

        aesCipher.init(Cipher.ENCRYPT_MODE, secKey);

        byte[] byteCipherText = aesCipher.doFinal(plainText.getBytes());

        return byteCipherText;

    }

     


    public static String decryptText(byte[] byteCipherText, SecretKey secKey) throws Exception {

        // AES defaults to AES/ECB/PKCS5Padding in Java 7

        Cipher aesCipher = Cipher.getInstance("AES");

        aesCipher.init(Cipher.DECRYPT_MODE, secKey);

        byte[] bytePlainText = aesCipher.doFinal(byteCipherText);

        return new String(bytePlainText);

    }

    public static SecretKey getSecretEncryptionKey() throws Exception{
    	
    	        KeyGenerator generator = KeyGenerator.getInstance("AES");
    	
    	        generator.init(128); // The AES key size in number of bits
    	
    	        SecretKey secKey = generator.generateKey();
    	
    	        return secKey;
    	
    	    }



    


}
