package com.prop.nextplaceprediction;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class DisplayService extends Activity {

	ListView listView;
	Connection conn;
	Double lat,lon;
	protected LocationManager mlocManager;
	String lati,longi,service;
	double lativalueup,lativaluedown,logivalueleft,logivalueright;
	String sendername;
	HashMap<String,String> usersList1 = null;
	HashMap<String,String> usersList = null;
	ArrayList<HashMap<String,String>> usersList2 = new ArrayList<HashMap<String,String>>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_display_services);
listView = (ListView) findViewById(R.id.listView1);

		
		
		Intent intent=getIntent();
		lati=intent.getStringExtra("lati").toString();
		longi=intent.getStringExtra("longi").toString();
		service=intent.getStringExtra("value").toString();
		Log.d("Latitudefriend",lati);
		Log.d("Longifriend",longi);
		Log.d("Loginname",service);
		
		try{
			 
					  
		
		double lativalueup=Double.valueOf(lati.trim()).doubleValue();
		lativalueup=lativalueup+(0.555500);
		Log.d("Latitudeup",""+lativalueup);
		
		double lativaluedown=Double.valueOf(lati.trim()).doubleValue()-(0.555500);
				
		Log.d("Latitudedown",""+lativaluedown);
		
		double longivalueleft=Double.valueOf(longi.trim()).doubleValue()-(0.555500);
		
		
		double longivalueright=Double.valueOf(longi.trim()).doubleValue()+(0.555500);
		
		
		Log.d("Longitudeleft",""+longivalueleft);
		Log.d("Longitudedown",""+longivalueright);
		String Text = " " +"latitudeup = "+ lativalueup + "Longitudeleft = " + longivalueleft;

		Toast.makeText( getApplicationContext(),Text,Toast.LENGTH_SHORT).show();
		
		new QuerySQL().execute(lativalueup,lativaluedown,longivalueleft,longivalueright,service);
		}
		catch (NumberFormatException e){
			  System.out.println("NumberFormatException: " + e.getMessage());
			  }
	
	}
	public class QuerySQL extends AsyncTask<Object, Void, Boolean> {

		ProgressDialog pDialog ;
		Exception error;
		
		
		ResultSet rs;
	    @Override
	    protected void onPreExecute() {
	        super.onPreExecute();
	        
	        pDialog = new ProgressDialog(DisplayService.this);
	        pDialog.setTitle("Services");
	        pDialog.setMessage("Get Services...");
	        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	        pDialog.setIndeterminate(false);
	        pDialog.setCancelable(false);
	        pDialog.show();
	    }

	    @Override
	    protected Boolean doInBackground(Object... args) {
	    	
	    	String lativalueup =  args[0].toString();
	    	String lativaluedown = args[1].toString();
	    	String logivalueleft =  args[2].toString();
	    	String logivalueright = args[3].toString();
	    	
	    	String service1=new String(args[4].toString());
	    	
			
			try {
				
				Class.forName("com.mysql.jdbc.Driver");
				conn = DriverManager.getConnection("jdbc:mysql://103.10.235.220:3306/nextplace","root","password");		
				
			} catch (SQLException se) {
				Log.e("ERRO1",se.getMessage());
			} catch (ClassNotFoundException e) {
				Log.e("ERRO2",e.getMessage());
			} catch (Exception e) {
			    Log.e("ERRO3",e.getMessage());
			}
			

			try {
				
				Log.d("Up",lativalueup);
				Log.d("Down ",lativaluedown);
				Log.d("Left ",logivalueleft);
				Log.d("Right ",logivalueright );
			Log.d("Service ",service1.trim());
			String servicename = "";
			int c5=1;
			String COMANDOSQL2="select * from cachetable ORDER BY id DESC";
			Statement statement2 = conn.createStatement();
			ResultSet rs2 = statement2.executeQuery(COMANDOSQL2);
		while(rs2.next()){
		if(c5==5)
		{
			servicename = rs2.getString(1);
			Statement statement1 = conn.createStatement();
			int success1=statement1.executeUpdate("insert into cachetable(servername) values('"+servicename+"')");
		
		
		}
		c5++;
		}
			
			
			int i=0,n;
			String COMANDOSQL1="select * from services where lati<='"+lativalueup+"' && lati>='"+lativaluedown+"'&& longi>='"+logivalueleft+"' && longi<='"+logivalueright+"' && servicename='"+service+"'";
			Statement statement1 = conn.createStatement();
			rs = statement1.executeQuery(COMANDOSQL1);
			  usersList = new HashMap<String, String>();
			 
			while(rs.next()){
				Log.d("Service ",service1.trim());
				double lon1=Double.parseDouble(rs.getString(3));
				double lon2=Double.parseDouble(longi);
				double theta = lon1 - lon2;
				  double dist = Math.sin(deg2rad(Double.parseDouble(rs.getString(2)))) * Math.sin(deg2rad(Double.parseDouble(lati))) + Math.cos(deg2rad(Double.parseDouble(rs.getString(2)))) * Math.cos(deg2rad(Double.parseDouble(lati))) * Math.cos(deg2rad(theta));
				  dist = Math.acos(dist);
				  dist = rad2deg(dist);
	
				      dist = dist * 1.609344;
				      usersList.put(String.valueOf(dist),rs.getString(1));
				      i++;
				Log.d("Distance",""+String.valueOf(dist));
				
				
			}
			 Log.d("Friend List Map1 :",usersList.toString());
			
					
			 n=i;
				Log.d("n",""+String.valueOf(n));
				
				
				Map<String, String> treeMap = new TreeMap<String, String>(usersList);
				
				
				Iterator iterator = treeMap.entrySet().iterator();
				while (iterator.hasNext()) {
					Map.Entry mapEntry = (Map.Entry) iterator.next();
					
					Log.d("mapvalue",mapEntry.getValue().toString());
					
					
					String COMANDOSQL="select * from services where id='"+mapEntry.getValue()+"'";
					Statement statement = conn.createStatement();
					rs = statement.executeQuery(COMANDOSQL);
				if(rs.next()){
					usersList1 = new HashMap<String, String>();			
					usersList1.put("name",rs.getString(5));                                    
					usersList1.put("addr",rs.getString(6));	
					usersList1.put("phone",rs.getString(7));	
					usersList1.put("emailid",rs.getString(8));
					usersList1.put("servicename",servicename);
				//	usersList1.put("phone",rs.getString(6));	
				//	usersList1.put("timings",rs.getString(7));	
					
		            Log.d("Friend List Map2 :",usersList1.toString());
		  	
		            usersList2.add(usersList1);
					
				
				}
				}
				
				
				return true;
				// Toast.makeText(getBaseContext(),
				// "Successfully Inserted.", Toast.LENGTH_LONG).show();
			} catch (Exception e) {
				error = e;
				return false;
//				Toast.makeText(getBaseContext(),"Successfully Registered...", Toast.LENGTH_LONG).show();
			}


	    }

	    private double deg2rad(double deg) {
	    	  return (deg * Math.PI / 180.0);
	    	}
	    private double rad2deg(double rad) {
	    	  return (rad * 180.0 / Math.PI);
	    	}
	    @SuppressLint("NewApi")
		@Override
	    protected void onPostExecute(Boolean result1) {
	    	pDialog.dismiss ( ) ;
	    	if(result1)
	    	{
                
			
					
//					System.out.println("ELSE(JSON) LOOP EXE");
					try {// try3 open
						
						listView.setAdapter(new CustomBaseAdapter(DisplayService.this, usersList2));
						listView.setOnItemClickListener(new OnItemClickListener() {

							public void onItemClick(AdapterView<?> parent, View v,
									int position, long id) {
								
							/*	Intent intent = new Intent(
										DisplayService.this,
										Message.class);
								intent.putExtra("name", usersList2.get(position)
										.get("name"));
								intent.putExtra("mobile",
										usersList2.get(position).get("mobile"));
								intent.putExtra("sendername",loginname);
								startActivity(intent);
						*/	}
						});			
						
					} catch (Exception e1) {
						Toast.makeText(getBaseContext(), e1.toString(),
								Toast.LENGTH_LONG).show();

					}					
				
            
	    	}else
	    	{
	    		if(error!=null)
	    		{
	    			//Toast.makeText(getBaseContext(),error.getMessage().toString() ,Toast.LENGTH_LONG).show();
	    		}
	    	}
	    	super.onPostExecute(result1);
	    }
	}
	


}
