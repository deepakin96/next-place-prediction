package com.prop.nextplaceprediction;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


public class CustomBaseAdapter extends BaseAdapter {
	Context con;
	LayoutInflater layoutInflater;
	ArrayList<HashMap<String,String>> listvalue;

	public CustomBaseAdapter(DisplayService listOfFriendsActivity,
			ArrayList<HashMap<String,String>> usersList) {
		// TODO Auto-generated constructor stub
		con = listOfFriendsActivity;
		listvalue = usersList;
		layoutInflater = LayoutInflater.from(listOfFriendsActivity);
	}

	

	public int getCount() {
		// TODO Auto-generated method stub
		return listvalue.size();
	}

	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return listvalue.get(position);
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.serviceview, null);
			viewHolder = new ViewHolder();
			viewHolder.txt1 = (TextView) convertView
					.findViewById(R.id.textView1);
			viewHolder.txt2 = (TextView) convertView
					.findViewById(R.id.textView2);
			viewHolder.txt3 = (TextView) convertView
					.findViewById(R.id.textView3);
			viewHolder.txt4 = (TextView) convertView
					.findViewById(R.id.textView4);
			
			viewHolder.txt5 = (TextView) convertView
					.findViewById(R.id.textView5);
			
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		Random rand = new Random(); 
		int value = rand.nextInt(10);
		String server = "Server "+value;
		
		viewHolder.txt1.setText(listvalue.get(position).get("name")
				.toString());
		viewHolder.txt2.setText(listvalue.get(position)
				.get("addr").toString());
		viewHolder.txt3.setText(listvalue.get(position)
				.get("phone").toString());
		viewHolder.txt4.setText(listvalue.get(position)
				.get("emailid").toString());
		viewHolder.txt5.setText(listvalue.get(position)
				.get("servicename").toString());
		
		return convertView;

	}

	class ViewHolder {
		TextView txt1, txt2,txt3,txt4,txt5;

	}

}


