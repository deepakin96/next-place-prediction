package com.prop.nextplaceprediction;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AdminActivity extends Activity {

	EditText edtName, edtMobileNo, edtEmail, edtCity, edtCountry, edtUserName,
	edtPassword, edtConfrimPassword;
Button btnSubmit;
Connection conn;
Double lat,lon;

protected LocationManager mlocManager;

private String name, mobilenumber, email, city, country, username,
	password, confrimpassword,lati,longi;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register1);
		edtCity = (EditText) findViewById(R.id.ser_lati);
		
		edtCountry = (EditText) findViewById(R.id.ser_longi);
		edtEmail = (EditText) findViewById(R.id.ser_email);
		edtMobileNo = (EditText) findViewById(R.id.ser_phno);
		edtName = (EditText) findViewById(R.id.ser_name);
		edtPassword = (EditText) findViewById(R.id.ser_addr);
		edtUserName = (EditText) findViewById(R.id.ser_username);
		
		mlocManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

		LocationListener mlocListener = new MyLocationListener();


		mlocManager.requestLocationUpdates( LocationManager.GPS_PROVIDER, 1, 1000, mlocListener);

//		lati=String.valueOf(lat);
//		longi=String.valueOf(lon);
//		if(lati.equals(null)||longi.equals(null) || lati.equals("") || longi.equals("")){
//			lati="71.000112";
//			longi="92.123343";
//		}
		btnSubmit = (Button) findViewById(R.id.ser_btn_reg);
		btnSubmit.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				name = edtName.getText().toString();
				mobilenumber = edtMobileNo.getText().toString();
				email = edtEmail.getText().toString();
				city = edtCity.getText().toString();
				country = edtCountry.getText().toString();
				username = edtUserName.getText().toString();
				password = edtPassword.getText().toString();
				
				
				Location location = mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
				
				if(location != null){
					lati = String.valueOf(location.getLatitude());
					longi = String.valueOf(location.getLongitude());
				}
			
				//	conn = CONN();
						try {
							new QuerySQL().execute();
							
//							Statement statement = conn.createStatement();
//							int success=statement.executeUpdate("insert into register values('"+name+"','"+mobilenumber+"','"+email+"','"+city+"','"+country+"','"+username+"','"+password+"','"+lati+"','"+longi+"')");
//						
//							if (success >= 1) {
//								// successfully created product
//								Intent i = new Intent(getApplicationContext(),
//										LoginFormActivity.class);
//								startActivity(i);
	//
//								// closing this screen
//								finish();
//							} else {
//								// failed to create product
//							}
							} catch (Exception e) {
				        Log.e("ERRO",e.getMessage());
						}

					
			}
		});

	}
	
//	private Connection CONN()
//	{
//
//		Connection conn = null;
//		String ConnURL = null;
//		try {
//		
//			
//			Class.forName("com.mysql.jdbc.Driver");
//			conn = DriverManager.getConnection("jdbc:mysql://ec2-23-21-211-172.compute-1.amazonaws.com:3306/eshadow","eshadowroot","password");				
//		} catch (SQLException se) {
//			Log.e("ERRO1",se.getMessage());
//		} catch (ClassNotFoundException e) {
//			Log.e("ERRO2",e.getMessage());
//		} catch (Exception e) {
//		    Log.e("ERRO3",e.getMessage());
//		}
//		return conn;
//	}



	
	
	public class QuerySQL extends AsyncTask<String, Void, Boolean> {

		ProgressDialog pDialog ;
		Exception error;
		ResultSet rs;
	    @Override
	    protected void onPreExecute() {
	        super.onPreExecute();
	        
	        pDialog = new ProgressDialog(AdminActivity.this);
	        pDialog.setTitle("Registration");
	        pDialog.setMessage("Registering your service...");
	        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	        pDialog.setIndeterminate(false);
	        pDialog.setCancelable(false);
	        pDialog.show();
	    }

	    @Override
	    protected Boolean doInBackground(String... args) {
	    	
	    	
	    	
	    	Location location = mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			
			if(location != null){
				lati = String.valueOf(location.getLatitude());
				longi = String.valueOf(location.getLongitude());
			}
			
			try {
				
				
				Class.forName("com.mysql.jdbc.Driver");
				conn = DriverManager.getConnection("jdbc:mysql://103.10.235.220:3306/nextplace","root","password");		
			} catch (SQLException se) {
				Log.e("ERRO1",se.getMessage());
			} catch (ClassNotFoundException e) {
				Log.e("ERRO2",e.getMessage());
			} catch (Exception e) {
			    Log.e("ERRO3",e.getMessage());
			}
			

			try {
				String one="o";
				Statement statement = conn.createStatement();
				int success=statement.executeUpdate("insert into services(lati,longi,servicename,name,addr,mobile,emailid) values('"+city+"','"+country+"','"+name+"','"+username+"','"+password+"','"+mobilenumber+"','"+email+"')");
			
				if (success >= 1) {
					// successfully created product
					
					return true;
					// closing this screen
//					finish();
				} else {
					// failed to create product
					return false;
				}


				
				// Toast.makeText(getBaseContext(),
				// "Successfully Inserted.", Toast.LENGTH_LONG).show();
			} catch (Exception e) {
				error = e;
				return false;
//				Toast.makeText(getBaseContext(),"Successfully Registered...", Toast.LENGTH_LONG).show();
			}


	    }

	    @SuppressLint("NewApi")
		@Override
	    protected void onPostExecute(Boolean result1) {
	    	pDialog.dismiss ( ) ;
	    	if(result1)
	    	{
                
	    		Toast.makeText(getBaseContext(),"Successfully created your service." ,Toast.LENGTH_LONG).show();
					
//					System.out.println("ELSE(JSON) LOOP EXE");
					try {// try3 open
						
						Intent i = new Intent(getApplicationContext(),
								AdminActivity.class);
						startActivity(i);		
						
					} catch (Exception e1) {
						Toast.makeText(getBaseContext(), e1.toString(),
								Toast.LENGTH_LONG).show();

					}					
				
            
	    	}else
	    	{
	    		if(error!=null)
	    		{
	    			Toast.makeText(getBaseContext(),error.getMessage().toString() ,Toast.LENGTH_LONG).show();
	    			Log.d("Error not null...", error.getMessage().toString());
	    		}
	    		else
	    		{
	    			Toast.makeText(getBaseContext(),"Not crreated your credentials!!!" ,Toast.LENGTH_LONG).show();
	    		}
	    	}
	    	super.onPostExecute(result1);
	    }
	}

/* Class My Location Listener */

public class MyLocationListener implements LocationListener

{

public void onLocationChanged(Location loc)

{

loc.getLatitude();

loc.getLongitude();

String Text = "My current location is: " +"latitude = "+ loc.getLatitude() + "Longitude = " + loc.getLongitude();

lat=loc.getLatitude();
 lon=loc.getLongitude();

Toast.makeText( getApplicationContext(),Text,Toast.LENGTH_SHORT).show();

}




public void onProviderDisabled(String provider)

{

Toast.makeText( getApplicationContext(),"Gps Disabled",Toast.LENGTH_SHORT ).show();

}




public void onProviderEnabled(String provider)

{

Toast.makeText( getApplicationContext(), "Gps Enabled", Toast.LENGTH_SHORT).show();

}


public void onStatusChanged(String provider, int status, Bundle extras)

{


}



}/* End of Class MyLocationListener */


}
