package com.prop.nextplaceprediction;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.json.JSONException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AdminFormActivity extends Activity {

	Connection conn;
	EditText username,password;
	Button signin,signup,adm1;
	String user,pass,lati="71.000112",longi="92.123343",user1,pass1;
	Double lat,lon;
	
	protected LocationManager mlocManager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.admin_login_form);
		signin=(Button)findViewById(R.id.admin_signin);
		
		adm1=(Button)findViewById(R.id.user_login);
	
		
		username=(EditText)findViewById(R.id.ad_username);
		password=(EditText)findViewById(R.id.ad_password);
//		conn=CONN();
		signin.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				user=username.getText().toString();
				pass=password.getText().toString();
				Log.d("username",user);
				Log.d("password",pass);
				 mlocManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

					LocationListener mlocListener = new MyLocationListener();


					mlocManager.requestLocationUpdates( LocationManager.GPS_PROVIDER, 1, 1000, mlocListener);

			/*	lati=String.valueOf(lat);
				longi=String.valueOf(lon);
				if(lati.equals(null)||longi.equals(null)){
					lati="71.000112";
					longi="92.123343";*/
			//	}
				new QuerySQL().execute(user,pass);
				
			}
			
		});
		
		
		adm1.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(AdminFormActivity.this,
						LoginFormActivity.class));			
				
			}
			
		});
	}
//	@SuppressLint("NewApi")
//	private Connection CONN()
//	{
//
//		Connection conn = null;
//		String ConnURL = null;
//		try {
//		
//			
//			Class.forName("com.mysql.jdbc.Driver");
//			conn = DriverManager.getConnection("jdbc:mysql://ec2-23-21-211-172.compute-1.amazonaws.com:3306/eshadow","eshadowroot","password");		
//		} catch (SQLException se) {
//			Log.e("ERRO1",se.getMessage());
//		} catch (ClassNotFoundException e) {
//			Log.e("ERRO2",e.getMessage());
//		} catch (Exception e) {
//		    Log.e("ERRO3",e.getMessage());
//		}
//		return conn;
//	}
	
//	public void QuerySQL(String user,String pass){
////		System.out.println("User Name : "+user+" Password : "+pass);
//		ResultSet rs;
//		Location location = mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//		
//		if(location != null){
//			lati = String.valueOf(location.getLatitude());
//			longi = String.valueOf(location.getLongitude());
//		}
//	
//		try {
//		String COMANDOSQL="select * from register where username='"+user+"' && password='"+pass+"'";
//			Statement statement = conn.createStatement();
//			rs = statement.executeQuery(COMANDOSQL);
//		while(rs.next()){
//			String COMANDOSQL1="update register set latitude='"+lati+"',longitude='"+longi+"' where username='"+user+"' && password='"+pass+"'";
//			Statement statement1 = conn.createStatement();
//			statement1.executeUpdate(COMANDOSQL1);
//			Intent intent=new Intent(LoginFormActivity.this,
//					MainActivity.class);
//			intent.putExtra("latitude", lati);
//			intent.putExtra("longitude", longi);
//			intent.putExtra("loginuser", user);
//		
//			startActivity(intent);
//		}
//
//			} catch (Exception e) {
//        Log.e("ERRO",e.getMessage());
//		}
//		
//	}
	
	
	
	
	public class QuerySQL extends AsyncTask<String, Void, Boolean> {

		ProgressDialog pDialog ;
		Exception error;
		ResultSet rs;
	    @Override
	    protected void onPreExecute() {
	        super.onPreExecute();
	        
	        pDialog = new ProgressDialog(AdminFormActivity.this);
	        pDialog.setTitle("Authentication");
	        pDialog.setMessage("Verifying your credentials...");
	        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	        pDialog.setIndeterminate(false);
	        pDialog.setCancelable(false);
	        pDialog.show();
	    }

	    @Override
	    protected Boolean doInBackground(String... args) {
	    	
	    	user1 = new String(args[0]);
	    	pass1 = new String(args[1]);
	    	
	    	Location location = mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			
			if(location != null){
				lati = String.valueOf(location.getLatitude());
				longi = String.valueOf(location.getLongitude());
			}
			
			try {
				
				
				Class.forName("com.mysql.jdbc.Driver");
				conn = DriverManager.getConnection("jdbc:mysql://103.10.235.220:3306/nextplace","root","password");		
			} catch (SQLException se) {
				Log.e("ERRO1",se.getMessage());
			} catch (ClassNotFoundException e) {
				Log.e("ERRO2",e.getMessage());
			} catch (Exception e) {
			    Log.e("ERRO3",e.getMessage());
			}
			

			try {
				String COMANDOSQL="select * from admintable where username='"+user1+"' && password='"+pass1+"'";
				Statement statement = conn.createStatement();
				rs = statement.executeQuery(COMANDOSQL);
			if(rs.next()){
				return true;
			}

return false;
				
				// Toast.makeText(getBaseContext(),
				// "Successfully Inserted.", Toast.LENGTH_LONG).show();
			} catch (Exception e) {
				error = e;
				return false;
//				Toast.makeText(getBaseContext(),"Successfully Registered...", Toast.LENGTH_LONG).show();
			}


	    }

	    @SuppressLint("NewApi")
		@Override
	    protected void onPostExecute(Boolean result1) {
	    	pDialog.dismiss ( ) ;
	    	if(result1)
	    	{
                
			
					
//					System.out.println("ELSE(JSON) LOOP EXE");
					try {// try3 open
						
						Intent intent=new Intent(AdminFormActivity.this,
								AdminActivity.class);
						
						startActivity(intent);			
						
					} catch (Exception e1) {
						Toast.makeText(getBaseContext(), e1.toString(),
								Toast.LENGTH_LONG).show();

					}					
				
            
	    	}else
	    	{
	    		if(error!=null)
	    		{
	    			Toast.makeText(getBaseContext(),error.getMessage().toString() ,Toast.LENGTH_LONG).show();
	    		}
	    		else
	    		{
	    			Toast.makeText(getBaseContext(),"Check your credentials!!!" ,Toast.LENGTH_LONG).show();
	    		}
	    	}
	    	super.onPostExecute(result1);
	    }
	}
	
	
	/* Class My Location Listener */

	public class MyLocationListener implements LocationListener

	{

	public void onLocationChanged(Location loc)

	{

	loc.getLatitude();

	loc.getLongitude();

	String Text = "My current location in login is: " +"latitude = "+ loc.getLatitude() + "Longitude = " + loc.getLongitude();

	lat=loc.getLatitude();
	 lon=loc.getLongitude();

	Toast.makeText( getApplicationContext(),Text,Toast.LENGTH_SHORT).show();

	}




	public void onProviderDisabled(String provider)

	{

	Toast.makeText( getApplicationContext(),"Gps Disabled",Toast.LENGTH_SHORT ).show();

	}




	public void onProviderEnabled(String provider)

	{

	Toast.makeText( getApplicationContext(), "Gps Enabled", Toast.LENGTH_SHORT).show();

	}


	public void onStatusChanged(String provider, int status, Bundle extras)

	{


	}



	}/* End of Class MyLocationListener */


}
