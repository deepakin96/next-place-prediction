package com.prop.nextplaceprediction;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class AddDailyActivity extends Activity {

	EditText edtName, edtMobileNo, edtEmail, edtCity, edtCountry, edtUserName,edt1,edt2,edt3,edt4,edt5,edt6,
	edtPassword, edtConfrimPassword;
Button btnSubmit,btnSubmit1;
Connection conn;
Double lat,lon;
static Cipher cipher;
protected LocationManager mlocManager;

private String name, mobilenumber, email, city, country, username,str1,str2,str3,str4,str5,str6,
	password, confrimpassword,lati,longi;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dailyadd);
		
		
		edt1 = (EditText) findViewById(R.id.register_name);
		edt2 = (EditText) findViewById(R.id.register_username);
		edt3 = (EditText) findViewById(R.id.register_password);
		edt4 = (EditText) findViewById(R.id.register_confirmPassword);
		edt5 = (EditText) findViewById(R.id.register_email);
		edt6 = (EditText) findViewById(R.id.register_phno);
		
		
		btnSubmit = (Button) findViewById(R.id.register_btn_reg);
		btnSubmit.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				str1 = edt1.getText().toString();
				str2 = edt2.getText().toString();
				str3 = edt3.getText().toString();
				str4 = edt4.getText().toString();
				str5 = edt5.getText().toString();
				str6 = edt6.getText().toString();
				
				
				if(verify())
				{
						try {
							new QuerySQL().execute();
							
//							Statement statement = conn.createStatement();
//							int success=statement.executeUpdate("insert into register values('"+name+"','"+mobilenumber+"','"+email+"','"+city+"','"+country+"','"+username+"','"+password+"','"+lati+"','"+longi+"')");
//						
//							if (success >= 1) {
//								// successfully created product
//								Intent i = new Intent(getApplicationContext(),
//										LoginFormActivity.class);
//								startActivity(i);
	//
//								// closing this screen
//								finish();
//							} else {
//								// failed to create product
//							}
							} catch (Exception e) {
				        Log.e("ERRO",e.getMessage());
						}

			}
			}
		});
		
		btnSubmit = (Button) findViewById(R.id.register_btn_cancel);
		btnSubmit.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				startActivity(new Intent(AddDailyActivity.this,
						MainActivity.class));	
			}
		
	});

	}
	
//	private Connection CONN()
//	{
//
//		Connection conn = null;
//		String ConnURL = null;
//		try {
//		
//			
//			Class.forName("com.mysql.jdbc.Driver");
//			conn = DriverManager.getConnection("jdbc:mysql://ec2-23-21-211-172.compute-1.amazonaws.com:3306/eshadow","eshadowroot","password");				
//		} catch (SQLException se) {
//			Log.e("ERRO1",se.getMessage());
//		} catch (ClassNotFoundException e) {
//			Log.e("ERRO2",e.getMessage());
//		} catch (Exception e) {
//		    Log.e("ERRO3",e.getMessage());
//		}
//		return conn;
//	}

	public boolean verify()
	{
//		EditText name, userName, password, cpassword, email, phoneNumber;
		Boolean ret=true;
		if(edt1.getText().toString().length()<1){edt1.setError("Field Required");ret=false;}
		if(edt2.getText().toString().length()<1){edt2.setError("Field Required");ret=false;}
		if(edt3.getText().toString().length()<1){edt3.setError("Field Required");ret=false;}
		if(edt4.getText().toString().length()<1){edt4.setError("Field Required");ret=false;}
		if(edt5.getText().toString().length()<1){edt5.setError("Field Required");ret=false;}
		if(edt6.getText().toString().length()<1){edt6.setError("Field Required");ret=false;}
				
		
		return ret;
	}
	

	
	
	public class QuerySQL extends AsyncTask<String, Void, Boolean> {

		ProgressDialog pDialog ;
		Exception error;
		ResultSet rs;
	    @Override
	    protected void onPreExecute() {
	        super.onPreExecute();
	        
	        pDialog = new ProgressDialog(AddDailyActivity.this);
	        pDialog.setTitle("Daily Activities");
	        pDialog.setMessage("Adding daily activities...");
	        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	        pDialog.setIndeterminate(false);
	        pDialog.setCancelable(false);
	        pDialog.show();
	    }

	    @Override
	    protected Boolean doInBackground(String... args) {
	    	
	    
			
			try {
				
				
				Class.forName("com.mysql.jdbc.Driver");
				conn = DriverManager.getConnection("jdbc:mysql://103.10.235.220:3306/nextplace","root","password");		
			} catch (SQLException se) {
				Log.e("ERRO1",se.getMessage());
			} catch (ClassNotFoundException e) {
				Log.e("ERRO2",e.getMessage());
			} catch (Exception e) {
			    Log.e("ERRO3",e.getMessage());
			}
			

			try {
				
				SharedPreferences preferences1=getSharedPreferences("username", Context.MODE_PRIVATE);
				username=preferences1.getString("username",null);

				String query = "update activitytable set earlymorning = ?,morning = ?,afternoon = ?,evening = ?,night = ?,latenight = ? where username = ?";
			      PreparedStatement preparedStmt = conn.prepareStatement(query);
			      preparedStmt.setString(1, str1);
			      preparedStmt.setString(2, str2);
			      preparedStmt.setString(3, str3);
			      preparedStmt.setString(4, str4);
			      preparedStmt.setString(5, str5);
			      preparedStmt.setString(6, str6);
			      preparedStmt.setString(7, username);

			      // execute the java preparedstatement
			      preparedStmt.executeUpdate();
				
				return true;

				
				// Toast.makeText(getBaseContext(),
				// "Successfully Inserted.", Toast.LENGTH_LONG).show();
			} catch (Exception e) {
				error = e;
				return false;
//				Toast.makeText(getBaseContext(),"Successfully Registered...", Toast.LENGTH_LONG).show();
			}


	    }

	    @SuppressLint("NewApi")
		@Override
	    protected void onPostExecute(Boolean result1) {
	    	pDialog.dismiss ( ) ;
	    	if(result1)
	    	{
                
	    		Toast.makeText(getBaseContext(),"Successfully updated the activities." ,Toast.LENGTH_LONG).show();
					
//					System.out.println("ELSE(JSON) LOOP EXE");
					try {// try3 open
						
						Intent i = new Intent(getApplicationContext(),
								LoginFormActivity.class);
						startActivity(i);		
						
					} catch (Exception e1) {
						Toast.makeText(getBaseContext(), e1.toString(),
								Toast.LENGTH_LONG).show();

					}					
				
            
	    	}else
	    	{
	    		if(error!=null)
	    		{
	    			Toast.makeText(getBaseContext(),error.getMessage().toString() ,Toast.LENGTH_LONG).show();
	    			Log.d("Error not null...", error.getMessage().toString());
	    		}
	    		else
	    		{
	    			Toast.makeText(getBaseContext(),"Not crreated your credentials!!!" ,Toast.LENGTH_LONG).show();
	    		}
	    	}
	    	super.onPostExecute(result1);
	    }
	}

/* Class My Location Listener */

public class MyLocationListener implements LocationListener

{

public void onLocationChanged(Location loc)

{

loc.getLatitude();

loc.getLongitude();

String Text = "My current location is: " +"latitude = "+ loc.getLatitude() + "Longitude = " + loc.getLongitude();

lat=loc.getLatitude();
 lon=loc.getLongitude();

Toast.makeText( getApplicationContext(),Text,Toast.LENGTH_SHORT).show();

}




public void onProviderDisabled(String provider)

{

Toast.makeText( getApplicationContext(),"Gps Disabled",Toast.LENGTH_SHORT ).show();

}




public void onProviderEnabled(String provider)

{

Toast.makeText( getApplicationContext(), "Gps Enabled", Toast.LENGTH_SHORT).show();

}


public void onStatusChanged(String provider, int status, Bundle extras)

{


}



}/* End of Class MyLocationListener */
public static byte[] encryptText(String plainText,SecretKey secKey) throws Exception{

        // AES defaults to AES/ECB/PKCS5Padding in Java 7

        Cipher aesCipher = Cipher.getInstance("AES");

        aesCipher.init(Cipher.ENCRYPT_MODE, secKey);

        byte[] byteCipherText = aesCipher.doFinal(plainText.getBytes());

        return byteCipherText;

    }

     


    public static String decryptText(byte[] byteCipherText, SecretKey secKey) throws Exception {

        // AES defaults to AES/ECB/PKCS5Padding in Java 7

        Cipher aesCipher = Cipher.getInstance("AES");

        aesCipher.init(Cipher.DECRYPT_MODE, secKey);

        byte[] bytePlainText = aesCipher.doFinal(byteCipherText);

        return new String(bytePlainText);

    }

    public static SecretKey getSecretEncryptionKey() throws Exception{
    	
    	        KeyGenerator generator = KeyGenerator.getInstance("AES");
    	
    	        generator.init(128); // The AES key size in number of bits
    	
    	        SecretKey secKey = generator.generateKey();
    	
    	        return secKey;
    	
    	    }



    


}
