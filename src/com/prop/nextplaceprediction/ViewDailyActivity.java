package com.prop.nextplaceprediction;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONException;


import com.prop.nextplaceprediction.LoginFormActivity.QuerySQL;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("NewApi")
public class ViewDailyActivity extends Activity {

	Connection conn;
	EditText username,password;
	Button signin,signup,adm1,btn1;
	String user,pass,lati="71.000112",longi="92.123343",user1,pass1, result1,result2,result3;
	Double lat,lon;
	TextView t1;
	protected LocationManager mlocManager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nextplaceview);
		
		t1 = (TextView) findViewById(R.id.textView1);
		
		btn1=(Button)findViewById(R.id.button1);
		
		btn1.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				
				
			
		
		SharedPreferences preferences1=getSharedPreferences("username", Context.MODE_PRIVATE);
		user1=preferences1.getString("username",null);

		DateFormat dateFormat = new SimpleDateFormat("HH");
		Date date = new Date();
		System.out.println(dateFormat.format(date));
		
		int hm = Integer.parseInt(dateFormat.format(date));
		
		if(hm>3 && hm<7)
		{
			result1="1";
		}
		
		if(hm>7 && hm<11)
		{
			result1="2";
		}
		
		if(hm>11 && hm<15)
		{
			result1="3";
		}
		
		if(hm>15 && hm<19)
		{
			result1="4";
		}
		
		if(hm>19 && hm<23)
		{
			result1="5";
		}
		
		if(hm>23 && hm<3)
		{
			result1="6";
		}
		
		//result1="";
		new QuerySQL().execute();
			}
			
		});
		
		
	}

	public class QuerySQL extends AsyncTask<String, Void, Boolean> {

		ProgressDialog pDialog ;
		Exception error;
		ResultSet rs;
	    @Override
	    protected void onPreExecute() {
	        super.onPreExecute();
	        
	        pDialog = new ProgressDialog(ViewDailyActivity.this);
	        pDialog.setTitle("Daily Activities");
	        pDialog.setMessage("View Next Place...");
	        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	        pDialog.setIndeterminate(false);
	        pDialog.setCancelable(false);
	        pDialog.show();
	    }

	    @Override
	    protected Boolean doInBackground(String... args) {
	    	
	    	
			
			try {
				
				
				Class.forName("com.mysql.jdbc.Driver");
				conn = DriverManager.getConnection("jdbc:mysql://103.10.235.220:3306/nextplace","root","password");		
			} catch (SQLException se) {
				Log.e("ERRO1",se.getMessage());
			} catch (ClassNotFoundException e) {
				Log.e("ERRO2",e.getMessage());
			} catch (Exception e) {
			    Log.e("ERRO3",e.getMessage());
			}
			
			result2 = result1;
			System.out.println("Result : "+result2);
			try {
				String COMANDOSQL="select * from activitytable where username='"+user1+"'";
				Statement statement = conn.createStatement();
				rs = statement.executeQuery(COMANDOSQL);
			if(rs.next()){
				if(result2.equals("1"))
				{
					result3 = rs.getString(2);
				}
				if(result2.equals("2"))
				{
					result3 = rs.getString(3);
				}
				if(result2.equals("3"))
				{
					result3 = rs.getString(4);
				}
				if(result2.equals("4"))
				{
					result3 = rs.getString(5);
				}
				if(result2.equals("5"))
				{
					result3 = rs.getString(6);
				}
				if(result2.equals("6"))
				{
					result3 = rs.getString(1);
				}
				
				
				return true;
			}

				return false;
				
				// Toast.makeText(getBaseContext(),
				// "Successfully Inserted.", Toast.LENGTH_LONG).show();
			} catch (Exception e) {
				error = e;
				return false;
//				Toast.makeText(getBaseContext(),"Successfully Registered...", Toast.LENGTH_LONG).show();
			}


	    }

	    @SuppressLint("NewApi")
		@Override
	    protected void onPostExecute(Boolean result1) {
	    	pDialog.dismiss ( ) ;
	    	if(result1)
	    	{
                
	    		t1.setText("The Next Place is : "+result3);
					
//					System.out.println("ELSE(JSON) LOOP EXE");
					try {// try3 open
						
					//	Intent intent=new Intent(ViewDailyActivity.this,MainActivity.class);
						
						
						
						//startActivity(intent);			
						
					} catch (Exception e1) {
						Toast.makeText(getBaseContext(), e1.toString(),
								Toast.LENGTH_LONG).show();

					}					
				
            
	    	}else
	    	{
	    		if(error!=null)
	    		{
	    			Toast.makeText(getBaseContext(),error.getMessage().toString() ,Toast.LENGTH_LONG).show();
	    		}
	    		else
	    		{
	    			Toast.makeText(getBaseContext(),"Check your credentials!!!" ,Toast.LENGTH_LONG).show();
	    		}
	    	}
	    	super.onPostExecute(result1);
	    }
	}
	
	
	/* Class My Location Listener */

	public class MyLocationListener implements LocationListener

	{

	public void onLocationChanged(Location loc)

	{

	loc.getLatitude();

	loc.getLongitude();

	String Text = "My current location in login is: " +"latitude = "+ loc.getLatitude() + "Longitude = " + loc.getLongitude();

	lat=loc.getLatitude();
	 lon=loc.getLongitude();

	Toast.makeText( getApplicationContext(),Text,Toast.LENGTH_SHORT).show();

	}




	public void onProviderDisabled(String provider)

	{

	Toast.makeText( getApplicationContext(),"Gps Disabled",Toast.LENGTH_SHORT ).show();

	}




	public void onProviderEnabled(String provider)

	{

	Toast.makeText( getApplicationContext(), "Gps Enabled", Toast.LENGTH_SHORT).show();

	}


	public void onStatusChanged(String provider, int status, Bundle extras)

	{


	}



	}/* End of Class MyLocationListener */


}
