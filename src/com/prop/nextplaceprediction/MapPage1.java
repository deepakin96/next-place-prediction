package com.prop.nextplaceprediction;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapPage1 extends FragmentActivity{

	LatLng fromPosition;
	MarkerOptions markerOptions;
	SupportMapFragment supportMapFragment;
	GoogleMap mGoogleMap;
	ArrayList<String> arr_name=new ArrayList<String>();
	ArrayList<String> arr_lat=new ArrayList<String>();
	ArrayList<String> arr_lng=new ArrayList<String>();
	
	Connection conn;
	
	String lati, longi;
	double lativalueup, lativaluedown, logivalueleft, logivalueright;
	String sendername;
	double fromlat,fromlong;
	String service1,username;
	
	
	GPSTracker gpstracker;
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.mappage1);
		
		Intent intent=getIntent();
		
		SharedPreferences preferences1=getSharedPreferences("username", Context.MODE_PRIVATE);
		username=preferences1.getString("username",null);

		
		//service1=intent.getStringExtra("value").toString();
		getWindow().setWindowAnimations(0);
		
		if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}
		
		Get_Current_GeoPoints();
		
		
//		district2 = intent.getStringExtra("district");
//		state2 = intent.getStringExtra("state");

//		try {
//			new QuerySQL().execute();
//		} catch (Exception e) {
//			Log.e("ERRO", e.getMessage());
//		}
		
	}
	
	private void Get_Current_GeoPoints()
	{
		gpstracker = new GPSTracker(MapPage1.this);

		if (gpstracker.canGetLocation()) {

			fromlat = gpstracker.getLatitude();
			fromlong = gpstracker.getLongitude();

			System.out.println("the current lat is " + fromlat);
			System.out.println("the current long is "
					+ fromlong);
			
			Display_Route();

		} else {
			//showSettingsAlert();
		}
		
	}
	
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		System.out.println("inside activity for result");
		if (requestCode == 1) {

			System.out.println("inside activity for result request code");
			gpstracker = new GPSTracker(MapPage1.this);

			if (gpstracker.canGetLocation()) {
				Get_Current_GeoPoints();
			} else {
				System.out.println("the gpstracker is disabled");
			//	showSettingsAlert();
			}

		}
	}
	
	public class QuerySQL extends AsyncTask<Object, Void, Boolean> {

		ProgressDialog pDialog;
		Exception error;

		ResultSet rs;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pDialog = new ProgressDialog(MapPage1.this);
			pDialog.setTitle("Next Place");
			pDialog.setMessage("Predicting Next Place...");
			pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Boolean doInBackground(Object... args) {

			try {

				Class.forName("com.mysql.jdbc.Driver");
				conn = DriverManager.getConnection(
						"jdbc:mysql://103.10.235.220:3306/nextplace", "root",
						"password");

			} catch (SQLException se) {
				Log.e("ERRO1", se.getMessage());
			} catch (ClassNotFoundException e) {
				Log.e("ERRO2", e.getMessage());
			} catch (Exception e) {
				Log.e("ERRO3", e.getMessage());
			}

			try {

//				String COMANDOSQL = "select * from bloodregister where bloodtype='"
//						+ bloodgroup2
//						+ "'&& district='"
//						+ district2
//						+ "'&& state='" + state2 + "' ";
				int count1=0;
				String COMANDOSQL = "select * from register where username='"+username+"'";
				
				Statement statement = conn.createStatement();
				rs = statement.executeQuery(COMANDOSQL);
				
				arr_name.clear();
				arr_lat.clear();
				arr_lng.clear();
				
				String lat4="";
				String long4="";
				// Toast.makeText(getBaseContext(),"11111...", Toast.LENGTH_LONG).show();

				if(rs.next()) {
					
					String lat2="";
					String long2="";
					
					double lat1 = Double.parseDouble(rs.getString("latitude"));
                	double long1 = Double.parseDouble(rs.getString("longitude"));
                	
                	lat4=""+lat1;
                	long4=""+long1;
                	//Toast.makeText(getBaseContext(),"111"+lat1+","+long1, Toast.LENGTH_LONG).show();
                	double latup = lat1 + 0.01;
                    double latdown = lat1 -0.01;
                  //  int count=0;
                    String COMANDOSQL2="select * from locationupdation where username='"+username+"' && latitude>='"+latdown+"' && latitude<='"+latup+"'";
                    Statement statement2 = conn.createStatement();
                    ResultSet rs2 = statement2.executeQuery(COMANDOSQL2);
                    while(rs2.next()){
                    //	 Toast.makeText(getBaseContext(),"while entered...", Toast.LENGTH_LONG).show();
                    	//lat2=rs2.getString("latitude");
                		//long2=rs2.getString("longitude");
                    	
                    	int id = Integer.parseInt(rs2.getString("id"));
                    	
                    	int id2 = id + 1;
                    	
                    	String COMANDOSQL3 = "select * from locationupdation where id='"+id2+"'";
        				
        				Statement statement3 = conn.createStatement();
        				ResultSet rs3 = statement.executeQuery(COMANDOSQL3);
                    	if(rs3.next())
                    	{
                    		// Toast.makeText(getBaseContext(),"count2...", Toast.LENGTH_LONG).show();
                    		double lat5 = Double.parseDouble(rs3.getString("latitude"));
                    		double long5 = Double.parseDouble(rs3.getString("longitude"));
                    		
                    		if(lat5>=latup && lat5<=latdown)
                    		{
                    			
                    		}
                    		else
                    		{
                    		lat2=rs3.getString("latitude");
                    		long2=rs3.getString("longitude");
                    		
                    		count1++;
                    		}
                    	}
                    	
                    	
					
				/*	String mobile=rs.getString("mobile");
					System.out.println("The mobile is "+mobile);
					arr_mobile.add(mobile);
					
					String lat=rs.getString("lati");
					System.out.println("The lat is "+lat);
					arr_lat.add(lat);
					
					String lng=rs.getString("longi");
					System.out.println("The lng is "+lng);
					arr_lng.add(lng);
					
					String name=rs.getString("name");
					System.out.println("The name is "+name);
					arr_name.add(name);
					
					String email=rs.getString("emailid");
					System.out.println("The email is "+email);
					arr_email.add(email);
					
					
					
					String gender=rs.getString("addr");
					System.out.println("The gender is "+gender);
					arr_gender.add(gender);
				*/
                    }
                  //  Toast.makeText(getBaseContext(),""+lat2+","+long2, Toast.LENGTH_LONG).show();
                    String name="Next Place";
					System.out.println("The name is "+name);
					arr_name.add(name);
                    String lat=lat2;
					System.out.println("The lat is "+lat);
					arr_lat.add(lat);
					
					String lng=long2;
					System.out.println("The lng is "+lng);
					arr_lng.add(lng);
                   
				}
				 if(count1>0)
                 {
                 return true;
                 }
				 else
				 {
					 String name="Current Place";
						System.out.println("The name is "+name);
						arr_name.add(name);
	                    String lat=lat4;
						System.out.println("The lat is "+lat);
						arr_lat.add(lat);
						
						String lng=long4;
						System.out.println("The lng is "+lng);
						arr_lng.add(lng);
				return true;
				 }
			} catch (Exception e) {
				error = e;
				return false;
				// Toast.makeText(getBaseContext(),"Successfully Registered...",
				// Toast.LENGTH_LONG).show();
			}

		}

		@SuppressLint("NewApi")
		@Override
		protected void onPostExecute(Boolean result1) {
			pDialog.dismiss();
			if (result1) {

				try 
				{
					if(arr_name.size()>=1)
					{
						System.out.println("Add mareker the list is not null");
						Add_Marker();
					}
					else
					{
						Toast.makeText(getBaseContext(), "No records to predict next place",
								Toast.LENGTH_LONG).show();
					}
				} 
				catch (Exception e1) 
				{
					Toast.makeText(getBaseContext(), e1.toString(),
							Toast.LENGTH_LONG).show();
				}

			} else {
				if (error != null) {
					 Toast.makeText(getBaseContext(),error.getMessage().toString()
					 ,Toast.LENGTH_LONG).show();
				}
			}
			super.onPostExecute(result1);
		}
	}

	private void Display_Route() {
		
		FragmentManager myFM = MapPage1.this.getSupportFragmentManager();

		supportMapFragment = (SupportMapFragment)myFM.findFragmentById(R.id.map);
		mGoogleMap = supportMapFragment.getMap();

		mGoogleMap.clear();
		mGoogleMap.setMyLocationEnabled(true);
		mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
		mGoogleMap.getUiSettings().setCompassEnabled(true);
		mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
		mGoogleMap.getUiSettings().setAllGesturesEnabled(true);
		mGoogleMap.setTrafficEnabled(true);
		
//		fromPosition = new LatLng(fromlat, fromlong);
//		mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
//				fromPosition, 12f));
		
//		markerOptions = new MarkerOptions();
//		markerOptions.draggable(true);
//		markerOptions.position(fromPosition).icon(
//		BitmapDescriptorFactory
//				.fromResource(R.drawable.maptomarker));

//		Add_Marker();
		
		mGoogleMap.setOnMarkerClickListener(new OnMarkerClickListener() {
			
			@Override
			public boolean onMarkerClick(Marker marker) {
				// TODO Auto-generated method stub
				
				String title=marker.getTitle().toString();
				
				System.out.println("You clicked " + title);

				for (int i = 0; i < arr_name.size(); i++) 
				{
					String name = arr_name.get(i);
					if (name.equals(title)) 
					{
						ShowInfo(i);
						break;
					}
				}
				
				return false;
			}
		});
		
		try {
			new QuerySQL().execute();
		} catch (Exception e) {
			Log.e("ERRO", e.getMessage());
		}
		
	}
	
	private void Add_Marker()
	{
		for(int i=0;i<arr_lat.size();i++)
		{
			double fromlat=Double.parseDouble(arr_lat.get(i));
			double fromlong=Double.parseDouble(arr_lng.get(i));
			String name=arr_name.get(i);
			
//			drawMarker(new LatLng(fromlat, fromlong));
			
			Draw_Marker(fromlat,fromlong,name);
			
		}
		
		fromPosition = new LatLng(fromlat, fromlong);
		mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
				fromPosition, 12f));
		
	}
	
	private void drawMarker(LatLng point){
        // Creating an instance of MarkerOptions
        MarkerOptions markerOptions = new MarkerOptions();
 
        // Setting latitude and longitude for the marker
        markerOptions.position(point);
 
        // Adding marker on the Google Map
        mGoogleMap.addMarker(markerOptions);
    }
	
	private void Draw_Marker(double fromlat, double fromlong, String name)
	{
		markerOptions = new MarkerOptions();
		markerOptions.draggable(true);
		fromPosition = new LatLng(fromlat, fromlong);
		markerOptions.position(fromPosition).icon(
				BitmapDescriptorFactory
						.fromResource(R.drawable.markerpin));
		mGoogleMap.addMarker(markerOptions).setTitle(name);
//		mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
//				fromPosition, 12f));

		
	}
	
	private void ShowInfo(int i)
	{
		Window mAlertDialogWindow;

		final AlertDialog dialog1 = new AlertDialog.Builder(MapPage1.this)
				.create();
		dialog1.show();

		dialog1.getWindow().clearFlags(
				WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
						| WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
		dialog1.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

		mAlertDialogWindow = dialog1.getWindow();
		View contv = LayoutInflater.from(MapPage1.this).inflate(
				R.layout.dialog_info1, null);
		contv.setFocusable(true);
		contv.setFocusableInTouchMode(true);

		mAlertDialogWindow
				.setBackgroundDrawableResource(R.drawable.material_dialog_window);

		mAlertDialogWindow.setContentView(contv);

		
		
	}
	
}
