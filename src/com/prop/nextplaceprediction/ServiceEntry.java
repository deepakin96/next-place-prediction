package com.prop.nextplaceprediction;


import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ServiceEntry extends Activity {

	EditText ed1;
	Button bt1;
	Button bt2;
	Intent intt;
	Intent intt1;
	String val;
	String user,pass,lati="10.999428",longi="77.083946",user1,pass1;
	Double lat,lon;
	LocationManager mlocManager ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_service_entry);
		ed1=(EditText)findViewById(R.id.editText1);
		bt1=(Button)findViewById(R.id.button1);
		bt2=(Button)findViewById(R.id.Button01);
		
		
    	
		/* mlocManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

			LocationListener mlocListener = new MyLocationListener();


			mlocManager.requestLocationUpdates( LocationManager.GPS_PROVIDER, 1, 1000, mlocListener);
		*/
    	intt=new Intent(this,DisplayService.class);
		
		bt1.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
						    	//Location location = mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
				
			/*	if(location != null){
					lati = String.valueOf(location.getLatitude());
					longi = String.valueOf(location.getLongitude());
				}*/
				GPSTracker gpsTracker=new GPSTracker(getApplicationContext());
		      	 
		    	lati = String.valueOf(gpsTracker.latitude);
		    	longi = String.valueOf(gpsTracker.longitude);	    	
				String Text = "My current location in login is: " +"latitude = "+ lati + "Longitude = " + longi;

				Toast.makeText( getApplicationContext(),Text,Toast.LENGTH_SHORT).show();

				val=ed1.getText().toString();
				intt.putExtra("value", val);
				intt.putExtra("lati", lati);
				intt.putExtra("longi", longi);
				startActivity(intt);
				
			}
			
		});
		bt2.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				val=ed1.getText().toString();
				Intent intent=new Intent(ServiceEntry.this,
						MapPage.class);
				intent.putExtra("value", val);
				startActivity(intent);		
				
			}
			
		});
		
	}
	


	

}
